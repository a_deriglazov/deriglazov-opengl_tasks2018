#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

MeshPtr makeCrossCap(unsigned int a, unsigned int N)
{
	unsigned int M = N / 2;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;

	for (unsigned int i = 0; i < M; i++)
	{
		float u = (float)glm::pi<float>() * i / M;
		float u1 = (float)glm::pi<float>() * (i + 1) / M;

		for (unsigned int j = 0; j < N; j++)
		{
			float v = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
			float v1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

			auto point1 = glm::vec3(a * a * (sin(u) * sin(2 * v) / 2), a * a * (sin(2 * u) * cos(v) * cos(v)), a*a*(cos(2 * u) * cos(v)*cos(v)));
			auto point2 = glm::vec3(a * a * (sin(u1) * sin(2 * v1) / 2), a * a * (sin(2 * u1) * cos(v1) * cos(v1)), a*a*(cos(2 * u1) * cos(v1)*cos(v1)));
			auto point3 = glm::vec3(a * a * (sin(u) * sin(2 * v1) / 2), a * a * (sin(2 * u) * cos(v1) * cos(v1)), a*a*(cos(2 * u) * cos(v1)*cos(v1)));
			auto point4 = glm::vec3(a * a * (sin(u1) * sin(2 * v) / 2), a * a * (sin(2 * u1) * cos(v) * cos(v)), a*a*(cos(2 * u1) * cos(v)*cos(v)));
			vertices.push_back(point1);
			vertices.push_back(point2);
			vertices.push_back(point3);

			normals.push_back(vectorsProduction(point2 - point1, point3 - point1));
			normals.push_back(vectorsProduction(point3 - point2, point1 - point2));
			normals.push_back(vectorsProduction(point1 - point3, point2 - point3));

			vertices.push_back(point1);
			vertices.push_back(point2);
			vertices.push_back(point4);

			normals.push_back(-vectorsProduction(point2 - point1, point4 - point1));
			normals.push_back(-vectorsProduction(point4 - point2, point1 - point2));
			normals.push_back(-vectorsProduction(point1 - point4, point2 - point4));

		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	std::cout << "Cross-cap is created with " << vertices.size() << " vertices\n";

	return mesh;
}

glm::vec3 vectorsProduction(glm::vec3 v1, glm::vec3 v2)
{
	auto product = glm::vec3((v1.y * v2.z - v2.y * v1.z), (v2.x * v1.z - v1.x * v2.z), (v1.x * v2.y - v1.y * v2.x));
	return glm::vec3(product / glm::length(product));
}