#include "Application.hpp"

#include <iostream>
#include <vector>
#include <cstdlib>

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Application* app = (Application*)glfwGetWindowUserPointer(window);
	app->handleKey(key, scancode, action, mods);
}

void windowSizeChangedCallback(GLFWwindow* window, int width, int height)
{
}

void mouseButtonPressedCallback(GLFWwindow* window, int button, int action, int mods)
{
}

void mouseCursosPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	Application* app = (Application*)glfwGetWindowUserPointer(window);
	app->handleMouseMove(xpos, ypos);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	Application* app = (Application*)glfwGetWindowUserPointer(window);
	app->handleScroll(xoffset, yoffset);
}

Application::Application(bool hasGUI) :
	_oldTime(0.0),
	_hasGUI(hasGUI),
	_cameraMover(std::make_shared<OrbitCameraMover>())
{
}

Application::~Application()
{
	if (_hasGUI)
	{
	}
	glfwTerminate();
}

void Application::start()
{
	initContext();

	initGL();

	if (_hasGUI)
	{
		initGUI();
	}

	makeScene();

	run();
}

void Application::initContext()
{
	if (!glfwInit())
	{
		std::cerr << "ERROR: could not start GLFW3\n";
		exit(1);
	}

#ifdef USE_CORE_PROFILE
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	int count;
	GLFWmonitor** monitors = glfwGetMonitors(&count);

	if (count == 2)
	{              
		_window = glfwCreateWindow(1920, 1080, "Cross-cap Surface", monitors[1], NULL);
	}
	else
	{
		_window = glfwCreateWindow(800, 600, "Cross-cap Surface", NULL, NULL);
	}
	if (!_window)
	{
		std::cerr << "ERROR: could not open window with GLFW3\n";
		glfwTerminate();
		exit(1);
	}
	glfwMakeContextCurrent(_window);

	glfwSwapInterval(0); 

	glfwSetWindowUserPointer(_window, this); 

	glfwSetKeyCallback(_window, keyCallback); 
	glfwSetWindowSizeCallback(_window, windowSizeChangedCallback);
	glfwSetMouseButtonCallback(_window, mouseButtonPressedCallback);
	glfwSetCursorPosCallback(_window, mouseCursosPosCallback);
	glfwSetScrollCallback(_window, scrollCallback);
}

void Application::initGL()
{
	glewExperimental = GL_TRUE;
	glewInit();

	const GLubyte* renderer = glGetString(GL_RENDERER); 
	const GLubyte* version = glGetString(GL_VERSION); 
	std::cout << "Renderer: " << renderer << std::endl;
	std::cout << "OpenGL version supported: " << version << std::endl;

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
}

void Application::makeScene()
{
	_camera.viewMatrix = glm::lookAt(glm::vec3(0.0f, -5.0f, 0.0f), glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	_camera.projMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.f);
}

void Application::run()
{
	while (!glfwWindowShouldClose(_window)) 
	{
		glfwPollEvents(); 

		update(); 

		if (_hasGUI)
		{
			updateGUI();
		}

		draw(); 

		if (_hasGUI)
		{
			drawGUI();
		}

		glfwSwapBuffers(_window); 
	}
}

void Application::handleKey(int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		if (key == GLFW_KEY_ESCAPE)
		{
			glfwSetWindowShouldClose(_window, GL_TRUE);
		}
	}

	_cameraMover->handleKey(_window, key, scancode, action, mods);
}

void Application::handleMouseMove(double xpos, double ypos)
{
	_cameraMover->handleMouseMove(_window, xpos, ypos);
}

void Application::handleScroll(double xoffset, double yoffset)
{
	_cameraMover->handleScroll(_window, xoffset, yoffset);
}

void Application::update()
{
	double dt = glfwGetTime() - _oldTime;
	_oldTime = glfwGetTime();

	_cameraMover->update(_window, dt);
	_camera = _cameraMover->cameraInfo();
}

void Application::draw()
{
}

void Application::initGUI()
{
}

void Application::updateGUI()
{
}

void Application::drawGUI()
{
}