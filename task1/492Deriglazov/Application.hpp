#pragma once

#include "Camera.hpp"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

class Application
{
public:
	Application(bool hasGUI = true);
	~Application();

	void start();

	virtual void handleKey(int key, int scancode, int action, int mods);

	virtual void handleMouseMove(double xpos, double ypos);

	virtual void handleScroll(double xoffset, double yoffset);

protected:
	virtual void initContext();

	virtual void initGL();

	virtual void initGUI();

	virtual void makeScene();

	void run();

	virtual void update();

	virtual void updateGUI();

	virtual void draw();

	virtual void drawGUI();


	GLFWwindow* _window;

	CameraInfo _camera;
	CameraMoverPtr _cameraMover;
	
	double _oldTime;

	bool _hasGUI;
};
