#include <Application.hpp>
#include "Mesh.hpp"
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

class SampleApplication : public Application
{
public:
	MeshPtr _crossCap;
	int detalisation = 100;

	ShaderProgramPtr _shader;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();

		_crossCap = makeCrossCap(1.2, detalisation);
		_crossCap->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

		_shader = std::make_shared<ShaderProgram>();
		_shader->createProgram(".\\Data\\shader.vert", ".\\Data\\shader.frag");
	}

	void update() override
	{
		Application::update();

		if(glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS)
		{
			if (detalisation < 10)
				return;
			--detalisation;
			_crossCap = makeCrossCap(1.2, detalisation);
		}
		if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
		{
			++detalisation;
			_crossCap = makeCrossCap(1.2, detalisation);
		}

		_crossCap->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_shader->setMat4Uniform("modelMatrix", _crossCap->modelMatrix());
		_crossCap->draw();
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}